# DevAssignment20181ST #

### What is this repository for? ###

This repository was set up as a repo to hold the assignment created by Steven Thewissen during his visit to DotControl on the 3rd of January. It contains the following features:

- Unit tests
- Some async code
- NuGet package creation through VSTS
- Build automation through VSTS